package zadanie11;

import java.util.ArrayList;

public class Tablica11 {
    public static void main(String[] args) {

        ArrayList<Integer> lista = new ArrayList<>();
        lista.add(23);
        lista.add(239);

        int wynik = sumaListy(lista);
        System.out.println("Suma = " + wynik);
        double wynik2 = sredniaListy(lista);
        System.out.println("Srednia = " + wynik2);
        double wynik3 = iloczynListy(lista);
        System.out.println("Iloczyn = " + wynik3);

    }

    public static Integer sumaListy(ArrayList<Integer> lista) {
        Integer suma = 0;
        for (int i = 0; i < lista.size(); i++) {
            suma += lista.get(i);
        }
        return suma;
    }

    public static double sredniaListy(ArrayList<Integer> lista) {
        int suma = 0;
        double srednia = 0;
        for (int i = 0; i < lista.size(); i++) {
            suma += lista.get(i);
            srednia = suma / lista.size();
        }
        return srednia;
    }
    public static double iloczynListy(ArrayList<Integer> lista) {
        double iloczyn = 1;
        for (int i = 0; i < lista.size(); i++) {
            iloczyn *= lista.get(i);
        }
        return iloczyn;
    }
}


