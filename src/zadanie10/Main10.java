package zadanie10;

import java.util.Scanner;

public class Main10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj wielkość pola:");
        int wielkoscPola = sc.nextInt();
        Field pole = new Field(wielkoscPola);


        boolean isWorking = true;
        while (isWorking) {
            String slowo = sc.next(); // jedno slowo
            if (slowo.equals("quit")) {
                isWorking = false;
                break;
            } else if (slowo.equals("check")) {
                System.out.println("Podaj X:");
                int x = sc.nextInt();
                System.out.println("Podaj Y:");
                int y = sc.nextInt();
                pole.checkCell(x, y);
            } else if (slowo.equals("print")) {
                pole.printField();
            }
        }

    }
}

