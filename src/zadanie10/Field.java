package zadanie10;



/**
 * Klasa reprezentująca planszę.
 */
public class Field {
    // rozmiar planszy
    // na samym początku po stworzeniu obiektu plansza bedzie miala rozmiar
    private int wielkoscPola; // = 0

    // nie mozemy przypisania zrobic
    // private boolean[][] pole = new boolean[wielkoscPola][wielkoscPola];
    // ^^ powyzsza instrukcja stworzy pole o wielkosci zero, poniewaz 'wielkoscPola' wynosi = 0

    private boolean[][] pole;

    /**
     * Inicjalizacja = stworzenie naszej planszy
     *
     * @param wielkoscPola - wielkość planszy
     */
    public Field(int wielkoscPola) {
        this.wielkoscPola = wielkoscPola;
        this.pole = new boolean[wielkoscPola][wielkoscPola];

        /*
        Domyśna wartość boolean jest false.
        Nie musimy tworzyć i przypisywać wartości.

        for (int i = 0; i < pole.length; i++) {
            for (int j = 0; j < pole[i].length; j++) {
                pole[i][j] = false;
            }
        }
        */
    }

    /**
     * Wypisanie całego pola.
     */
    public void printField() {
        // wypisanie wiersza indeksów kolumn
        System.out.print("   ");
        for (int i = 0; i < pole.length; i++) {
            System.out.print((i + 1) + " ");
        }
        // nowa linia po indeksach
        System.out.println();

        for (int i = 0; i < pole.length; i++) { // wiersze
            // wypisanie numeru wiersza
            System.out.print((i + 1) + "  ");
            for (int j = 0; j < pole[i].length; j++) { // kolumny w wierszach
                // jeśli zaznaczone
                if (pole[i][j]) {
                    System.out.print("X ");
                } else {
                    System.out.print("O ");
                }
            }
            // co wiersz zrób nową linię
            System.out.println();
        }
    }

    /**
     * Zaznacza komórkę o indeksie x i y. Wypisuje komunikat
     *
     * @param x - wspolrzedna x
     * @param y - wspolrzedna y
     */
    public void checkCell(int x, int y) {

        // aby zachować justowanie (indeks od 1 a nie od 0) odejmuje po 1 od zaznaczanego indeksu
        x = x-1;
        y = y-1;

        if (pole[x][y]) {
            // jest juz zaznaczone
            System.out.println("Jest juz zaznaczone");
        } else {
            pole[x][y] = true;
        }
    }

    public void setWielkoscPola(int wielkoscPola) {
        this.wielkoscPola = wielkoscPola;
        this.pole = new boolean[wielkoscPola][wielkoscPola];
    }

    public int getWielkoscPola() {
        return wielkoscPola;
    }
}
