package zadanie5;

public class Prostokąt {

    private int  b ;
    private int  c ;

    public Prostokąt (int b, int c){
        this.b = b;
        this.c = c;
    }
    public int obliczObwod() {
        return (2*b)+(2*c);
    }
    public int obliczPole() {
        return (b * c);
    }
}
