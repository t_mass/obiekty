package zadanie5;

public class Kwadrat {
    private int a;

    public Kwadrat(int a)
    {
        this.a = a;
    }

    public int obliczObwod() {
        return (4 * a);
    }

    public int obliczPole() {
        return (a * a);
    }
}
