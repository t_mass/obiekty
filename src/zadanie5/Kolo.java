package zadanie5;

public class Kolo {

    private int promien ;

    public Kolo (int promien){
        this.promien = promien;
    }
    public double obliczObwod() {
        return (2* Math.PI * promien);
    }
    public double obliczPole() {
        return ( Math.PI * (promien * promien));
}
}