package zadanie5;

public class Main {
    public static void main(String[] args) {
        Kwadrat kwadrat = new Kwadrat(7);
        System.out.println(kwadrat.obliczObwod());
        System.out.println(kwadrat.obliczPole());

        Prostokąt prostokąt = new Prostokąt(8, 26);
        System.out.println(prostokąt.obliczObwod());
        System.out.println(prostokąt.obliczPole());

        Kolo kolo = new Kolo(10);
        System.out.println(kolo.obliczObwod());
        System.out.println(kolo.obliczPole());
    }
}