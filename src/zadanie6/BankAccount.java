package zadanie6;

public class BankAccount {

    private double stan;

    public BankAccount(double stan) {
        this.stan = stan;
    }

    public void addMoney(double toAdd) {
        stan = stan + toAdd;
    }

    public void subtractMoney(double toSubtract) {
        stan = stan - toSubtract;
    }

    public double printBankAccountStatus () {
        return stan;


    }

}
