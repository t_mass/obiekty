package zadanie6;

public class Main6 {
    public static void main(String[] args) {

        BankAccount bankAccount = new BankAccount(100);

        bankAccount.addMoney(500);
        System.out.println();
        bankAccount.subtractMoney(700);
        System.out.println(bankAccount.printBankAccountStatus());

    }
}
