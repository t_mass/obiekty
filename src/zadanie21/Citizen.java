package zadanie21;

public abstract class Citizen {

    private String name;

    public abstract boolean canVote();

}
