package zadanie1;

public class Osoba {
    private String name;
    private int age;

    public Osoba(String imie,int age){
        this.name= imie;
        this.age= age;
    }

    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void printYourNameAndAge(){
        System.out.println(name + " " + age);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

